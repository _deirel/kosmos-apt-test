package ru.deirel.ferm {
	/**
     * ...
     * @author Deirel
     */
    public class StorageRecord {
        public var itemId:String;
        public var count:int;
        
        public function StorageRecord(itemId:String, count:int) {
            this.itemId = itemId;
            this.count = count;
        }
    }
}