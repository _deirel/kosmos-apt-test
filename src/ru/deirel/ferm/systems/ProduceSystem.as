package ru.deirel.ferm.systems {
    import ash.core.Engine;
    import ash.core.NodeList;
    import ash.tools.ListIteratingSystem;
	
	/**
     * ...
     * @author Deirel
     */
    public class ProduceSystem extends ListIteratingSystem {
        private var _storageList:NodeList;
        
        public function ProduceSystem() {
            super(_ProduceSystemNode, nodeUpdate);
        }
        
        override public function addToEngine(engine:Engine):void {
            super.addToEngine(engine);
            _storageList = engine.getNodeList(_StorageOnlyNode);
            _storageList.nodeRemoved.add(disposeStorage);
        }
        
        override public function removeFromEngine(engine:Engine):void {
            _storageList.nodeRemoved.remove(disposeStorage);
            _storageList = null;
            super.removeFromEngine(engine);
        }
        
        private function nodeUpdate(node:_ProduceSystemNode, dt:Number):void {
            if (!node.produce.isCyclic && node.storageComponent.wasCleaned) {
                node.storageComponent.wasCleaned = false;
                node.produce.reset(dt);
            }
            
            // First, we update production process
            var produceComplete:Boolean = node.produce.update(dt);
            
            // Second, we block it if necessary
            if (node.produce.wasBlocked) node.produce.reset(dt);
            
            // Third, we process the product.
            // It is intended when a hunger is the same time as a production
            if (produceComplete) onProduceComplete(node);
            
            node.produce.wasBlocked = false;
        }
        
        /**
         * We need to manually dispose storage component because it listen events of Storage
         */
        private function disposeStorage(node:_StorageOnlyNode):void {
            node.storageComponent.dispose();
        }
        
        private function onProduceComplete(node:_ProduceSystemNode):void {
            node.storageComponent.storage.add(node.produce.productId);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.ProduceComponent;
import ru.deirel.ferm.components.StorageComponent;

class _ProduceSystemNode extends Node {
    public var produce:ProduceComponent;
    public var storageComponent:StorageComponent;
}

class _StorageOnlyNode extends Node {
    public var storageComponent:StorageComponent;
}