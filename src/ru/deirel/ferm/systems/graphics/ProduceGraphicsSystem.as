package ru.deirel.ferm.systems.graphics {
    import ash.core.Engine;
    import ash.core.NodeList;
    import ash.core.System;
	
	/**
     * ...
     * @author Deirel
     */
    public class ProduceGraphicsSystem extends System {
        private var _entityNodeList:NodeList;
        private var _produceNodeList:NodeList;
        private var _bothNodeList:NodeList;
        
        public function ProduceGraphicsSystem() {
        }
        
        override public function addToEngine(engine:Engine):void {
            _entityNodeList = engine.getNodeList(_ProduceGraphicsEntityNode);
            _produceNodeList = engine.getNodeList(_ProduceGraphicsProduceNode);
            _bothNodeList = engine.getNodeList(_ProduceGraphicsBothNode);
            
            _bothNodeList.nodeAdded.add(nodeAdded);
        }
        
        override public function removeFromEngine(engine:Engine):void {
            _bothNodeList.nodeAdded.remove(nodeAdded);
            _entityNodeList = null;
            _produceNodeList = null;
            _bothNodeList = null;
            super.removeFromEngine(engine);
        }
        
        override public function update(time:Number):void {
            for (var nodeE:_ProduceGraphicsEntityNode = _entityNodeList.head; nodeE; nodeE = nodeE.next) {
                nodeE.entityGraphics.progress = nodeE.produce.progress;
            }
            for (var nodeP:_ProduceGraphicsProduceNode = _produceNodeList.head; nodeP; nodeP = nodeP.next) {
                nodeP.produceGraphics.progress = nodeP.produce.progress;
            }
        }
        
        private function nodeAdded(node:_ProduceGraphicsBothNode):void {
            node.produceGraphics.y = -5.5;
            node.entityGraphics.addChild(node.produceGraphics);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.EntityGraphicsComponent;
import ru.deirel.ferm.components.ProduceComponent;
import ru.deirel.ferm.components.ProduceGraphicsComponent;

class _ProduceGraphicsEntityNode extends Node {
    public var produce:ProduceComponent;
    public var entityGraphics:EntityGraphicsComponent;
}

class _ProduceGraphicsProduceNode extends Node {
    public var produce:ProduceComponent;
    public var produceGraphics:ProduceGraphicsComponent;
}

class _ProduceGraphicsBothNode extends Node {
    public var produce:ProduceComponent;
    public var entityGraphics:EntityGraphicsComponent;
    public var produceGraphics:ProduceGraphicsComponent;
}