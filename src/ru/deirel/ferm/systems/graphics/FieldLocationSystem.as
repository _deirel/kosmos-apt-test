package ru.deirel.ferm.systems.graphics {
    import ash.tools.ListIteratingSystem;
    import ru.deirel.ferm.Utils;
	
	/**
     * ...
     * @author Deirel
     */
    public class FieldLocationSystem extends ListIteratingSystem {
        public function FieldLocationSystem() {
            super(_FieldLocationNode, Utils.emptyNodeFunction, nodeAdded, nodeRemoved);
        }
        
        private function nodeAdded(node:_FieldLocationNode):void {
            node.fieldLocation.field.addEntity(node.fieldLocation.x, node.fieldLocation.y, node.entity);
        }
        
        private function nodeRemoved(node:_FieldLocationNode):void {
            node.fieldLocation.field.removeEntity(node.fieldLocation.x, node.fieldLocation.y);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.FieldLocationComponent;

class _FieldLocationNode extends Node {
    public var fieldLocation:FieldLocationComponent;
}