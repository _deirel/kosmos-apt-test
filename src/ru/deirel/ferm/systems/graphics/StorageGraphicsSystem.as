package ru.deirel.ferm.systems.graphics {
    import ash.core.Engine;
    import ash.core.NodeList;
    import ash.tools.ListIteratingSystem;
	/**
     * ...
     * @author Deirel
     */
    public class StorageGraphicsSystem extends ListIteratingSystem {
        private var _lst:NodeList;
        
        public function StorageGraphicsSystem() {
            super(_StorageGraphicsNode, nodeUpdate);
        }
        
        override public function addToEngine(engine:Engine):void {
            super.addToEngine(engine);
            _lst = engine.getNodeList(_StorageEntityGraphicsNode);
            _lst.nodeAdded.add(addToEntitySprite);
        }
        
        override public function removeFromEngine(engine:Engine):void {
            _lst.nodeAdded.remove(addToEntitySprite);
            _lst = null;
            super.removeFromEngine(engine);
        }
        
        private function nodeUpdate(node:_StorageGraphicsNode, dt:Number):void {
            if (node.storageComponent.wasChanged) {
                node.storageComponent.wasChanged = false;
                node.storageGraphics.updateByStorageRecords(node.storageComponent.storage.items);
            }
        }
        
        private function addToEntitySprite(node:_StorageEntityGraphicsNode):void {
            node.storageGraphics.x = 5.0;
            node.storageGraphics.y = 5.0;
            node.entityGraphics.addChild(node.storageGraphics);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.EntityGraphicsComponent;
import ru.deirel.ferm.components.StorageComponent;
import ru.deirel.ferm.components.StorageGraphicsComponent;

class _StorageGraphicsNode extends Node {
    public var storageComponent:StorageComponent;
    public var storageGraphics:StorageGraphicsComponent;
}

class _StorageEntityGraphicsNode extends Node {
    public var storageGraphics:StorageGraphicsComponent;
    public var entityGraphics:EntityGraphicsComponent;
}