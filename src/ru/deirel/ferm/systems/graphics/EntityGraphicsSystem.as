package ru.deirel.ferm.systems.graphics {
    import ash.tools.ListIteratingSystem;
    import ru.deirel.ferm.Utils;
	/**
     * ...
     * @author Deirel
     */
    public class EntityGraphicsSystem extends ListIteratingSystem {
        
        public function EntityGraphicsSystem() {
            super(_EntityGraphicsNode, Utils.emptyNodeFunction, nodeAdded);
        }
        
        private function nodeAdded(node:_EntityGraphicsNode):void {
            // Update entity graphics by data
            node.graphics.record = node.record.record;
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.EntityGraphicsComponent;
import ru.deirel.ferm.components.EntityRecordComponent;

class _EntityGraphicsNode extends Node {
    public var graphics:EntityGraphicsComponent;
    public var record:EntityRecordComponent;
}