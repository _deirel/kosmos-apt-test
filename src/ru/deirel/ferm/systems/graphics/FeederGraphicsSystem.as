package ru.deirel.ferm.systems.graphics {
    import ash.core.Engine;
    import ash.core.NodeList;
    import ash.tools.ListIteratingSystem;
	/**
     * ...
     * @author Deirel
     */
    public class FeederGraphicsSystem extends ListIteratingSystem {
        private var _lst:NodeList;
        
        public function FeederGraphicsSystem() {
            super(_FeederGraphicsNode, nodeUpdate);
        }
        
        override public function addToEngine(engine:Engine):void {
            super.addToEngine(engine);
            _lst = engine.getNodeList(_FeederEntityGraphicsNode);
            _lst.nodeAdded.add(addToEntitySprite);
        }
        
        override public function removeFromEngine(engine:Engine):void {
            _lst.nodeAdded.remove(addToEntitySprite);
            _lst = null;
            super.removeFromEngine(engine);
        }
        
        private function nodeUpdate(node:_FeederGraphicsNode, dt:Number):void {
            if (node.feeder.wasChanged) {
                node.feeder.wasChanged = false;
                node.feederGraphics.update(node.feeder.foodId, node.feeder.count);
            }
        }
        
        private function addToEntitySprite(node:_FeederEntityGraphicsNode):void {
            node.feederGraphics.x = -5.0;
            node.feederGraphics.y = 5.0;
            node.entityGraphics.addChild(node.feederGraphics);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.EntityGraphicsComponent;
import ru.deirel.ferm.components.FeederComponent;
import ru.deirel.ferm.components.FeederGraphicsComponent;

class _FeederGraphicsNode extends Node {
    public var feeder:FeederComponent;
    public var feederGraphics:FeederGraphicsComponent;
}

class _FeederEntityGraphicsNode extends Node {
    public var feederGraphics:FeederGraphicsComponent;
    public var entityGraphics:EntityGraphicsComponent;
}