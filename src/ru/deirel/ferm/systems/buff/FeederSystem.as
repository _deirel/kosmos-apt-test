package ru.deirel.ferm.systems.buff {
    import ash.tools.ListIteratingSystem;
	/**
     * ...
     * @author Deirel
     */
    public class FeederSystem extends ListIteratingSystem {
        public function FeederSystem() {
            super(_FeederNode, nodeUpdate);
        }
        
        public function nodeUpdate(node:_FeederNode, dt:Number):void {
            // This magic number '1' means that animals eat 1 item at a time
            // Here we reset a hunger before it will block production process
            if (node.hunger.isComplete && node.feeder.get(1)) node.hunger.reset(dt);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.FeederComponent;
import ru.deirel.ferm.components.HungerComponent;

class _FeederNode extends Node {
    public var feeder:FeederComponent;
    public var hunger:HungerComponent;
}