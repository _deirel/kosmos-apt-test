package ru.deirel.ferm.systems.debuff {
    import ash.tools.ListIteratingSystem;
	
	/**
     * ...
     * @author Deirel
     */
    public class HungerBlockSystem extends ListIteratingSystem {        
        public function HungerBlockSystem() {
            super(_HungerBlockNode, nodeUpdate);
        }
        
        private function nodeUpdate(node:_HungerBlockNode, dt:Number):void {
            if (node.hunger.isComplete) node.produce.wasBlocked = true;
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.HungerComponent;
import ru.deirel.ferm.components.ProduceComponent;

class _HungerBlockNode extends Node {
    public var hunger:HungerComponent;
    public var produce:ProduceComponent;
}