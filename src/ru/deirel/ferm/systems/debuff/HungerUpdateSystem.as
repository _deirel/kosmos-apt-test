package ru.deirel.ferm.systems.debuff {
    import ash.tools.ListIteratingSystem;
	
	/**
     * ...
     * @author Deirel
     */
    public class HungerUpdateSystem extends ListIteratingSystem {        
        public function HungerUpdateSystem() {
            super(_HungerUpdateNode, nodeUpdate);
        }
        
        private function nodeUpdate(node:_HungerUpdateNode, dt:Number):void {
            node.hunger.update(dt);
        }
    }
}

import ash.core.Node;
import ru.deirel.ferm.components.HungerComponent;

class _HungerUpdateNode extends Node {
    public var hunger:HungerComponent;
}