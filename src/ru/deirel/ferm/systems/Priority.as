package ru.deirel.ferm.systems {
	/**
     * Priority of systems is very important
     * @author Deirel
     */
    public class Priority {
        static public const ENTITY_GRAPHICS:int = 80;           // The first, setup the entity graphics
        static public const FIELD_LOCATION:int = 85;            // The second, add the entity to the field
        
        static public const HUNGER_UPDATE:int = 90;             // Then some logic updates
        //... Other systems that can block production
        
        static public const FEEDER:int = 100;              
        //... Other systems that can unblock production blocking systems
        
        static public const HUNGER_BLOCK:int = 110;
        //... Systems that control production blocking 
        
        static public const PRODUCE:int = 120;
        
        static public const PROCUDE_GRAPHICS:int = 130;         // And update graphics for results of that logic
        static public const STORAGE_GRAPHICS:int = 135;
        static public const FEEDER_GRAPHICS:int = 140;
    }
}