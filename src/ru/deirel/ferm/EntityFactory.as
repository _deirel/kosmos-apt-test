package ru.deirel.ferm {
    import ash.core.Entity;
    import ru.deirel.ferm.components.*;
    import ru.deirel.ferm.engine.Director;
	/**
     * This class provides methods to create entities
     * @author Deirel
     */
    public class EntityFactory {
        /// Create entity by id on the field with (x, y) position
        static public function createEntity(id:String, field:Field, x:int, y:int):Entity {
            var record:EntityRecord = EntityRecord.getById(id);
            return record.type == EntityRecord.TYPE_PLANT ? createPlant(id, field, x, y)
                 : record.type == EntityRecord.TYPE_ANIMAL ? createAnimal(id, field, x, y)
                 : null;
        }
        
        /// A set of components for a plant
        static private function createPlant(id:String, field:Field, x:int, y:int):Entity {
            var record:EntityRecord = EntityRecord.getById(id);
            return common(record, field, x, y)
            .add(new ProduceComponent(record.produceItemId, record.produceRate, false));
        }
        
        /// A set of components for an animal
        static private function createAnimal(id:String, field:Field, x:int, y:int):Entity {
            var record:EntityRecord = EntityRecord.getById(id);
            return common(record, field, x, y)
            .add(new HungerComponent(record.hungerRate))
            .add(new FeederComponent(record.foodId, 3))
            .add(new FeederGraphicsComponent(Director.instance.itemGraphicsProvider))
            .add(new ProduceComponent(record.produceItemId, record.produceRate, true))
            .add(new ProduceGraphicsComponent(Director.instance.guiGraphicsProvider))
            .add(new StorageGraphicsComponent(Director.instance.itemGraphicsProvider, Director.instance.guiGraphicsProvider));
        }
        
        /// A common set of components
        static private function common(record:EntityRecord, field:Field, x:int, y:int):Entity {
            return new Entity()
            .add(new EntityRecordComponent(record))
            .add(new EntityGraphicsComponent(Director.instance.entityGraphicsProvider))
            .add(new StorageComponent())
            .add(new FieldLocationComponent(x, y, field));
        }
    }
}