package ru.deirel.ferm {
    import ash.core.Entity;
    import flash.utils.Dictionary;
    import starling.events.EventDispatcher;
	/**
     * ...
     * @author Deirel
     */
    public class Field extends EventDispatcher {        
        static public const EVENT_ADD_ENTITY:String = "addEntity";
        static public const EVENT_REMOVE_ENTITY:String = "removeEntity";
        
        private var _width:int;
        private var _height:int;
        private var _cells:Dictionary = new Dictionary();
        
        public function Field(w:int, h:int) {
            _width = w;
            _height = h;
        }
        
        public function addEntity(x:int, y:int, e:Entity):void {
            var id:String = _id(x, y);
            if (_cells[id]) return;
            _cells[id] = e;
            dispatchEventWith(EVENT_ADD_ENTITY, false, { x:x, y:y, entity:e });
        }
        
        public function removeEntity(x:int, y:int):void {
            var id:String = _id(x, y);
            if (_cells[id]) {
                dispatchEventWith(EVENT_REMOVE_ENTITY, false, { x:x, y:y, entity:_cells[id] });
                delete _cells[id];
            }
        }
        
        public function getEntity(x:int, y:int):Entity {
            return _cells[_id(x, y)] as Entity;
        }
        
        private function _id(x:int, y:int):String {
            return x + ":" + y;
        }
        
        public function get width():int {
            return _width;
        }
        
        public function get height():int {
            return _height;
        }
    }
}