package ru.deirel.ferm {
    import ash.core.Engine;
    import ash.core.Entity;
    import feathers.controls.Callout;
    import feathers.core.FeathersControl;
    import ru.deirel.ferm.components.FeederComponent;
    import ru.deirel.ferm.components.StorageComponent;
    import ru.deirel.ferm.engine.Director;
    import ru.deirel.ferm.graphics.CellAction;
    import ru.deirel.ferm.graphics.CellCallout;
    import ru.deirel.ferm.graphics.EntityAction;
    import ru.deirel.ferm.graphics.EntityCallout;
    import ru.deirel.ferm.graphics.FieldSprite;
    import ru.deirel.ferm.graphics.MainStorageSprite;
    import ru.deirel.ferm.graphics.MoneyPanel;
    import ru.deirel.ferm.systems.*;
    import ru.deirel.ferm.systems.buff.*;
    import ru.deirel.ferm.systems.debuff.*;
    import ru.deirel.ferm.systems.graphics.*;
    import starling.display.DisplayObject;
    import starling.events.Event;
    import starling.events.EventDispatcher;
	/**
     * ...
     * @author Deirel
     */
    public class Game extends EventDispatcher {
        //static public const EVENT_
        
        private var _field:Field;
        private var _shop:Shop;
        private var _storage:Storage;
        
        private var _fieldGraphics:FieldSprite;
        private var _storageGraphics:MainStorageSprite;
        private var _moneyGraphics:MoneyPanel;
        
        private var _engine:Engine;
        private var _fixedStep:Number;
        
        private var _activeCallout:Callout;
        
        public function Game() {
            // Models of game elements
            _field = new Field(8, 8);
            _shop = new Shop();
            _storage = new Storage();
            
            // Views of game elements
            _fieldGraphics = new FieldSprite(_field, Director.instance.guiGraphicsProvider);
            _fieldGraphics.scale = Director.SCALE;
            
            _storageGraphics = new MainStorageSprite(_storage, Director.instance.itemGraphicsProvider, Director.instance.guiGraphicsProvider);
            
            _moneyGraphics = new MoneyPanel(_shop);
            
            initEvents();
            initEngine();
            
            _fixedStep = 1 / Director.instance.fps;
        }
        
        /**
         * Initialize main game logic
         */
        private function initEngine():void {
            // Use entity system architecture with Ash framework
            _engine = new Engine();
            
            // The systems contain game logic
            // Hunger logic
            _engine.addSystem(new HungerUpdateSystem(), Priority.HUNGER_UPDATE);
            _engine.addSystem(new HungerBlockSystem(), Priority.HUNGER_BLOCK);
            
            // Feeder logic
            _engine.addSystem(new FeederSystem(), Priority.FEEDER);
            
            // Produce items (milk, eggs, wheat etc) logic
            _engine.addSystem(new ProduceSystem(), Priority.PRODUCE);
            
            // Add to and remove entities from the field logic
            _engine.addSystem(new FieldLocationSystem(), Priority.FIELD_LOCATION);
            
            // Graphics systems
            // Update entity graphics by its data (EntityRecord)
            _engine.addSystem(new EntityGraphicsSystem(), Priority.ENTITY_GRAPHICS);
            
            // Update entity graphics and produce progress bars by production progress
            _engine.addSystem(new ProduceGraphicsSystem(), Priority.PROCUDE_GRAPHICS);
            
            // Update storage graphics
            _engine.addSystem(new StorageGraphicsSystem(), Priority.STORAGE_GRAPHICS);
            
            // Update feeder graphics
            _engine.addSystem(new FeederGraphicsSystem(), Priority.FEEDER_GRAPHICS);
            
            // Create some entities for example
            _engine.addEntity(EntityFactory.createEntity(EntityRecord.WHEAT, _field, 0, 0));
            _engine.addEntity(EntityFactory.createEntity(EntityRecord.CHICKEN, _field, 1, 0));
            _engine.addEntity(EntityFactory.createEntity(EntityRecord.COW, _field, 2, 0));
        }
        
        /**
         * Initialize events from views
         */
        private function initEvents():void {
            _fieldGraphics.addEventListener(FieldSprite.EVENT_ENTITY_TOUCH, onEntityTouch);
            _fieldGraphics.addEventListener(FieldSprite.EVENT_CELL_TOUCH, onCellTouch);
            _storageGraphics.addEventListener(MainStorageSprite.EVENT_SELL, onStorageSell);
        }
        
        /**
         * When touch an entity, show corresponding menu
         */
        private function onEntityTouch(e:Event):void {
            var entity:Entity = e.data.entity;
            _activeCallout = _fieldGraphics.createCalloutOnCell(e.data.x, e.data.y, new EntityCallout(Director.instance.guiGraphicsProvider, onEntityAction, entity, this));
            _activeCallout.disposeContent = true;
        }
        
        /**
         * When touch an emty cell, show corresponding menu
         */
        private function onCellTouch(e:Event):void {
            _activeCallout = _fieldGraphics.createCalloutOnCell(e.data.x, e.data.y, new CellCallout(Director.instance.guiGraphicsProvider, onCellAction, e.data.x, e.data.y));
            _activeCallout.disposeContent = true;
        }
        
        /**
         * When we want to sell item from storage
         */
        private function onStorageSell(e:Event):void {
            var itemId:String = e.data.id;
            if (_storage.take(itemId, 1)) {
                _shop.sell(new <StorageRecord>[new StorageRecord(itemId, 1)]);
            }
        }
        
        /**
         * Add entity choosed by user to the cell
         */
        private function onCellAction(data:CellAction):void {
            closeActiveCallout();
            _engine.addEntity(EntityFactory.createEntity(data.entityId, _field, data.cellX, data.cellY));
        }
        
        /**
         * Perform choosed by user action
         */
        private function onEntityAction(data:EntityAction):void {
            closeActiveCallout();
            switch (data.type) {
                case EntityAction.REMOVE_ENTITY:
                    _engine.removeEntity(data.entity);
                    break;
                case EntityAction.FEED:
                    if (_storage.take(data.record.foodId, 1)) {
                        var feeder:FeederComponent = data.entity.get(FeederComponent);
                        if (feeder) feeder.add(1);
                    }
                    break;
                case EntityAction.HARVEST:
                    var entityStorage:StorageComponent = data.entity.get(StorageComponent);
                    if (entityStorage && !entityStorage.storage.isEmpty) {
                        _storage.addMany(entityStorage.storage.takeAway());
                    }
                    break;
            }
        }
        
        private function closeActiveCallout():void {
            if (_activeCallout) {
                _activeCallout.close(true);
                _activeCallout = null;
            }
        }
        
        public function update():void {
            _engine.update(_fixedStep);
        }
        
        //--------------------------------------  Getters and setters -------------------------------------
        
        public function get fieldGraphics():DisplayObject {
            return _fieldGraphics;
        }
        
        public function get storage():Storage {
            return _storage;
        }
        
        public function get storageGraphics():FeathersControl {
            return _storageGraphics;
        }
        
        public function get moneyGraphics():FeathersControl {
            return _moneyGraphics;
        }
        
        public function dispose():void {
            if (_engine) {
                _engine.removeAllEntities();
                _engine.removeAllSystems();
                _engine = null;
            }
            
            _fieldGraphics = Utils.dispose(_fieldGraphics);
            _storageGraphics = Utils.dispose(_storageGraphics);
            _moneyGraphics = Utils.dispose(_moneyGraphics);
            
            _field = Utils.dispose(_field);
            _shop = Utils.dispose(_shop);
            _storage = Utils.dispose(_storage);
        }
    }
}