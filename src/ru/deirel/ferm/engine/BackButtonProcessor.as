package ru.deirel.ferm.engine {
    import flash.display.Stage;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
	/**
     * ...
     * @author Deirel
     */
    public class BackButtonProcessor {
        private var _handlersStack:Vector.<IBackButtonHandler> = new Vector.<IBackButtonHandler>();
        
        public function BackButtonProcessor(stage:Stage) {
            stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        }
        
        private function onKeyDown(e:KeyboardEvent):void {
            if (e.keyCode == Keyboard.BACK || e.keyCode == Keyboard.ESCAPE) {
                if (_handlersStack.length > 0) _handlersStack[0].onBackButton();
            }
        }
        
        public function addHandler(handler:IBackButtonHandler):void {
            _handlersStack.unshift(handler);
        }
        
        public function removeHandler(handler:IBackButtonHandler):void {
            var id:int = _handlersStack.indexOf(handler);
            if (id >= 0) _handlersStack.splice(id, 1);
        }
    }
}