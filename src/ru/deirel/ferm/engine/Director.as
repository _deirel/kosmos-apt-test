package ru.deirel.ferm.engine {
    import feathers.controls.ScreenNavigator;
    import ru.deirel.ferm.graphics.EntityGraphicsProvider;
    import ru.deirel.ferm.graphics.GUIGraphicsProvider;
    import ru.deirel.ferm.graphics.ItemGraphicsProvider;
    import starling.core.Starling;
	/**
     * Director class is a common service locator
     * @author Deirel
     */
    public class Director {
        static public const SCALE:Number = 4.0;
        
        static private var _instance:Director = null;
        
        static public function get instance():Director {
            return _instance ? _instance : _instance = new Director();
        }
        
        private var _native:INativeCapabilities;
        private var _fps:Number;
        
        private var _screenNavigator:ScreenNavigator;
        private var _backButtonProcessor:BackButtonProcessor;
        
        public var entityGraphicsProvider:EntityGraphicsProvider;
        public var itemGraphicsProvider:ItemGraphicsProvider;
        public var guiGraphicsProvider:GUIGraphicsProvider;
        
        public function Director() {
            if (!Director) throw new Error("Director is a singleton");
        }
        
        public function init(nativeCapabilities:INativeCapabilities):void {
            _native = nativeCapabilities;
            _native.init();
            _fps = Starling.current.nativeStage.frameRate;
            _screenNavigator = new ScreenNavigator();
            _backButtonProcessor = new BackButtonProcessor(Starling.current.nativeStage);
        }
        
        public function get screenNavigator():ScreenNavigator {
            return _screenNavigator;
        }
        
        public function get fps():Number {
            return _fps;
        }
        
        public function get isMobile():Boolean {
            return _native.isMobile();
        }
        
        public function get backButtonProcessor():BackButtonProcessor {
            return _backButtonProcessor;
        }
        
        public function end():void {
            _screenNavigator.dispose();
            _native.closeApp();
        }
    }
}