package ru.deirel.ferm.engine {
    
    /**
     * ...
     * @author Deirel
     */
    public interface INativeCapabilities {
        function init():void;
        function closeApp():void;
        function isMobile():Boolean;
    }
}