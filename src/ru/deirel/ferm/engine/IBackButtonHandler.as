package ru.deirel.ferm.engine {
	/**
     * ...
     * @author Deirel
     */
    public interface IBackButtonHandler {
        function onBackButton():void;
    }
}