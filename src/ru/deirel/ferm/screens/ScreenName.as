package ru.deirel.ferm.screens {
	/**
     * ...
     * @author Deirel
     */
    public class ScreenName {
        static public const INIT:String = "initScreen";
        static public const MENU:String = "menuScreen";
        static public const GAME:String = "gameScreen";
    }
}