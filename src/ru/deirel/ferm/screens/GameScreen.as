package ru.deirel.ferm.screens {
    import feathers.controls.Label;
    import feathers.controls.Screen;
    import feathers.controls.ScrollContainer;
    import feathers.core.PopUpManager;
    import feathers.layout.AnchorLayout;
    import feathers.layout.AnchorLayoutData;
    import ru.deirel.ferm.Game;
    import ru.deirel.ferm.Utils;
    import ru.deirel.ferm.engine.Director;
    import ru.deirel.ferm.engine.IBackButtonHandler;
    import ru.deirel.ferm.graphics.MainStorageSprite;
    import ru.deirel.ferm.graphics.PauseMenu;
    import starling.events.EnterFrameEvent;
	
	/**
     * ...
     * @author Deirel
     */
    public class GameScreen extends Screen implements IBackButtonHandler {
        private var _gameContainer:ScrollContainer;
        private var _game:Game;
        private var label:Label;
        
        public function GameScreen() {
            layout = new AnchorLayout();
        }
        
        override protected function initialize():void {
            super.initialize();
            
            _gameContainer = new ScrollContainer();
            _gameContainer.layoutData = new AnchorLayoutData(0.0, MainStorageSprite.WIDTH, 0.0, 0.0);
            addChild(_gameContainer);
            
            // Create a new game
            _game = new Game();
            
            // Add its graphics to the screen
            _game.fieldGraphics.x = _game.fieldGraphics.y = 10.0;
            _gameContainer.addChild(_game.fieldGraphics);
            
            _game.storageGraphics.layoutData = new AnchorLayoutData(0, 0, 0, NaN);
            addChild(_game.storageGraphics);
            
            _game.moneyGraphics.layoutData = new AnchorLayoutData(NaN, 10.0, 10.0, NaN);
            addChild(_game.moneyGraphics);
            
            // Add events handlers
            Director.instance.backButtonProcessor.addHandler(this);
            
            addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
        }
        
        private function onEnterFrame(e:EnterFrameEvent):void {
            _game.update();
        }
        
        public function onBackButton():void {
            PopUpManager.addPopUp(new PauseMenu());
        }
        
        override public function dispose():void {
            _game = Utils.dispose(_game);
            Director.instance.backButtonProcessor.removeHandler(this);
            super.dispose();
        }
    }
}