package ru.deirel.ferm.screens {
    import feathers.controls.List;
    import feathers.controls.Screen;
    import feathers.data.ListCollection;
    import feathers.layout.AnchorLayout;
    import feathers.layout.AnchorLayoutData;
    import feathers.motion.Fade;
    import ru.deirel.ferm.engine.Director;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class MenuScreen extends Screen {
        public function MenuScreen() {
            layout = new AnchorLayout();
        }
        
        override protected function initialize():void {
            super.initialize();
            
            var menu:List = new List();
            var items:Array = [{ text:"Новая игра", callback:newGameCb }];
            if (Director.instance.isMobile) items.push({ text:"Выход", callback:exitCb });
            menu.dataProvider = new ListCollection(items);
            menu.itemRendererProperties.labelField = "text";
            menu.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0.0, 0.0);
            addChild(menu); 
            
            menu.addEventListener(Event.CHANGE, onMenuListChange);
        }
        
        private function onMenuListChange(e:Event):void {
            var menu:List = List(e.currentTarget);
            menu.selectedItem.callback();
        }
        
        private function newGameCb():void {
            Director.instance.screenNavigator.showScreen(ScreenName.GAME, Fade.createCrossfadeTransition());
        }
        
        private function exitCb():void {
            Director.instance.end();
        }
    }
}