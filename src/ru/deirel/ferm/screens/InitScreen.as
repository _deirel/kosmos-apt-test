package ru.deirel.ferm.screens {
    import feathers.controls.LayoutGroup;
    import feathers.controls.Screen;
    import feathers.events.FeathersEventType;
    import feathers.layout.AnchorLayout;
    import feathers.layout.AnchorLayoutData;
    import feathers.motion.Fade;
    import ru.deirel.ferm.EntityRecord;
    import ru.deirel.ferm.engine.Director;
    import ru.deirel.ferm.graphics.EntityGraphicsProvider;
    import ru.deirel.ferm.graphics.GUIGraphicsProvider;
    import ru.deirel.ferm.graphics.ItemGraphicsProvider;
    import starling.display.Image;
    import starling.events.Event;
    import starling.textures.Texture;
    import starling.textures.TextureAtlas;
    import starling.textures.TextureSmoothing;
	/**
     * ...
     * @author Deirel
     */
    public class InitScreen extends Screen {
        [Embed(source = "../../../../../assets/logo.png")]
        static private const logoClass:Class;
        
        [Embed(source = "../../../../../assets/atlas.xml", mimeType = "application/octet-stream")]
        static private const atlasXMLClass:Class;
        
        [Embed(source = "../../../../../assets/atlas.png")]
        static private const atlasImageClass:Class;
        
        public function InitScreen() {
            layout = new AnchorLayout();
            addEventListener(FeathersEventType.TRANSITION_IN_COMPLETE, onTransitionComplete);
        }
        
        override protected function initialize():void {
            super.initialize();
            
            // Show logo
            var logo:Image = new Image(Texture.fromEmbeddedAsset(logoClass));
            logo.textureSmoothing = TextureSmoothing.NONE;
            logo.scale = Director.SCALE;
            logo.x = width / 2.0;
            logo.y = height / 2.0;
            logo.touchable = false;
            
            var group:LayoutGroup = new LayoutGroup();
            group.addChild(logo);
            group.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0.0, 0.0);
            addChild(group);
        }
        
        private function onTransitionComplete(e:Event):void {
            // Load and initialize resources
            removeEventListener(FeathersEventType.TRANSITION_IN_COMPLETE, onTransitionComplete);
            
            var atlasXML:XML = new XML(new atlasXMLClass());
            var atlasTexture:Texture = Texture.fromEmbeddedAsset(atlasImageClass);
            var atlas:TextureAtlas = new TextureAtlas(atlasTexture, atlasXML);
            
            // Graphics providers are classes which provides a graphics for different purposes
            Director.instance.entityGraphicsProvider = new EntityGraphicsProvider(atlas);
            Director.instance.itemGraphicsProvider = new ItemGraphicsProvider(atlas);
            Director.instance.guiGraphicsProvider = new GUIGraphicsProvider(atlas);
            
            EntityRecord.init(function():void {
                // Once finished, show Menu screen
                Director.instance.screenNavigator.showScreen(ScreenName.MENU, Fade.createCrossfadeTransition());
            });
        }
    }
}