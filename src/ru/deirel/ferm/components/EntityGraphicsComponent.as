package ru.deirel.ferm.components {
    import flash.geom.Point;
    import ru.deirel.ferm.EntityRecord;
    import ru.deirel.ferm.graphics.EntityGraphicsProvider;
    import ru.deirel.ferm.graphics.EntityGraphicsSprite;
    import starling.display.DisplayObject;
    import starling.display.Sprite;
	/**
     * Entity graphics
     * @author Deirel
     */
    public class EntityGraphicsComponent extends Sprite {
        private var _graphics:EntityGraphicsSprite;
        private var _provider:EntityGraphicsProvider;
        
        // Will be set externally (by Field class)
        public var fieldX:int, fieldY:int;
        
        public function EntityGraphicsComponent(provider:EntityGraphicsProvider) {
            _provider = provider;
        }
        
        public function set record(value:EntityRecord):void {
            if (_graphics) removeChild(_graphics);
            _graphics = _provider.getGraphicsByRecord(value);
            if (_graphics) addChildAt(_graphics, 0);
        }
        
        public function set progress(value:Number):void {
            _graphics.progress = value;
        }
        
        override public function hitTest(localPoint:Point):DisplayObject {
            var hitObject:DisplayObject = super.hitTest(localPoint);
            return (hitObject != null ? this : hitObject);
        }
    }
}