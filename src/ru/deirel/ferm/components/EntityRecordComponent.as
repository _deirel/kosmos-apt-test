package ru.deirel.ferm.components {
    import ru.deirel.ferm.EntityRecord;
	/**
     * Provides the data about the entity
     * @author Deirel
     */
    public class EntityRecordComponent {
        public var record:EntityRecord;
        
        public function EntityRecordComponent(rec:EntityRecord) {
            this.record = rec;
        }
    }
}