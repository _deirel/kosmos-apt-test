package ru.deirel.ferm.components {
	/**
     * Production internal logic
     * @author Deirel
     */
    public class ProduceComponent extends ProgressComponent {
        public var productId:String;
        public var wasBlocked:Boolean = false;
        
        public function ProduceComponent(productId:String, rate:Number, cyclic:Boolean = false) {
            super(rate, cyclic);
			this.productId = productId;
        }
    }
}