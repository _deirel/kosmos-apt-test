package ru.deirel.ferm.components {
    import ru.deirel.ferm.StorageRecord;
    import ru.deirel.ferm.graphics.GUIGraphicsProvider;
    import ru.deirel.ferm.graphics.ItemGraphicsProvider;
    import starling.display.DisplayObject;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.TextureSmoothing;
	
	/**
     * Storage graphics
     * @author Deirel
     */
    public class StorageGraphicsComponent extends Sprite {
        static private const MAX_VISIBLE_COUNT:int = 3;
        
        private var _itemsGraphicsProvider:ItemGraphicsProvider;
        private var _guiGraphicsProvider:GUIGraphicsProvider;
        
        public function StorageGraphicsComponent(itemsGraphicsProvider:ItemGraphicsProvider, guiGraphicsProvider:GUIGraphicsProvider) {
            _itemsGraphicsProvider = itemsGraphicsProvider;
            _guiGraphicsProvider = guiGraphicsProvider;
        }
        
        public function updateByStorageRecords(items:Vector.<StorageRecord>):void {
            removeChildren(0, -1, true);
            if (!items.length) return;
            var record:StorageRecord = items[0];
            for (var i:int = 0; i < MAX_VISIBLE_COUNT; i++) {
                if (i >= record.count) break;
                var graphics:DisplayObject = _itemsGraphicsProvider.getGraphicsById(record.itemId);
                graphics.scale = 0.75;
                graphics.x = graphics.y = 0.75 * i;
                graphics.touchable = false;
                addChild(graphics);
            }
            if (record.count > MAX_VISIBLE_COUNT) {
                var plusImage:Image = new Image(_guiGraphicsProvider.getTexture("plus"));
                plusImage.textureSmoothing = TextureSmoothing.NONE;
                plusImage.scale = 0.75;
                plusImage.alignPivot();
                plusImage.x = plusImage.y = 3;
                plusImage.touchable = false;
                addChild(plusImage);
            }
        }
    }
}