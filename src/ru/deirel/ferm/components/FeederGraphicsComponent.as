package ru.deirel.ferm.components {
    import ru.deirel.ferm.graphics.ItemGraphicsProvider;
    import starling.display.DisplayObject;
    import starling.display.Sprite;
	/**
     * Feeder graphics
     * @author Deirel
     */
    public class FeederGraphicsComponent extends Sprite {
        private var _provider:ItemGraphicsProvider;
        
        public function FeederGraphicsComponent(provider:ItemGraphicsProvider) {
            _provider = provider;
        }
        
        public function update(foodId:String, count:int):void {
            removeChildren(0, -1, true);
            for (var i:int = 0; i < count; i++) {
                var graphics:DisplayObject = _provider.getGraphicsById(foodId);
                graphics.touchable = false;
                graphics.x = graphics.y = 0.75 * i;
                addChild(graphics);
            }
        }
    }
}