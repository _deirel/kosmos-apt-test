package ru.deirel.ferm.components {
	/**
     * Hunger internal logic
     * @author Deirel
     */
    public class HungerComponent extends ProgressComponent {        
        public function HungerComponent(rate:Number) {
            super(rate, false);
        }
    }
}