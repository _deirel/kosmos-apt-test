package ru.deirel.ferm.components {
    import flash.utils.Dictionary;
    import ru.deirel.ferm.Storage;
    import starling.events.Event;
	/**
     * Storage component internal logic
     * Use Storage class and make it applicable for Ash Systems
     * @author Deirel
     */
    public class StorageComponent {    
        public var wasChanged:Boolean = false;
        public var wasCleaned:Boolean = false;
        
        private var _storage:Storage;
        
        private var _items:Dictionary = new Dictionary();
        private var _isEmpty:Boolean = true;
        
        public function StorageComponent() {
            _storage = new Storage();
            _storage.addEventListener(Storage.EVENT_CHANGE, onChange);
            _storage.addEventListener(Storage.EVENT_CLEAN, onClean);
        }
        
        private function onChange(e:Event):void {
            wasChanged = true;
        }
        
        private function onClean(e:Event):void {
            wasCleaned = true;
        }
        
        public function get storage():Storage {
            return _storage;
        }
        
        public function dispose():void {
            _storage.dispose();
            _storage = null;
        }
    }
}