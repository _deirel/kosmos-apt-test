package ru.deirel.ferm.components {
    import flash.geom.Rectangle;
    import ru.deirel.ferm.graphics.GUIGraphicsProvider;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.TextureSmoothing;
	/**
     * Visualization of production process, it's a progress bar
     * @author Deirel
     */
    public class ProduceGraphicsComponent extends Sprite {
        private var _base:Image;
        private var _line:Image;
        
        public function ProduceGraphicsComponent(provider:GUIGraphicsProvider) {
            _base = new Image(provider.getTexture("progress_base"));
            _base.scale9Grid = new Rectangle(1, 1, 1, 1);
            _base.width = 14.0;
            _base.textureSmoothing = TextureSmoothing.NONE;
            addChild(_base);
            
            _line = new Image(provider.getTexture("progress_bar"));
            _line.x = _line.y = 1;
            _line.textureSmoothing = TextureSmoothing.NONE;
            addChild(_line);
            
            alignPivot();
        }
        
        public function set progress(value:Number):void {
            _line.width = value * (_base.width - 2);
        }
    }
}