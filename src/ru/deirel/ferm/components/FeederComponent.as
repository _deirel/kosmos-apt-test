package ru.deirel.ferm.components {
	/**
     * Feeder internal logic
     * @author Deirel
     */
    public class FeederComponent {        
        private var _foodId:String;
        private var _maxCount:int;
        
        public var count:int;
        
        public var wasChanged:Boolean = false;
        
        public function FeederComponent(foodId:String, maxCount:int) {
            _foodId = foodId;
            _maxCount = maxCount;
        }
        
        /**
         * Add 'howMush' of food to the feeder
         * @param howMuch how much food to add
         * @return how much food actually added
         */ 
        public function add(howMuch:int):int {
            var oldCount:int = count;
            count += howMuch;
            if (count > _maxCount) count = _maxCount;
            wasChanged = count != oldCount;
            return count - oldCount;
        }
        
        /**
         * Try to take 'howMuch' of food from the feeder
         * @param howMuch how much food want to take
         * @return true if the food was enough
         */
        public function get(howMuch:int = 1):Boolean {
            if (count >= howMuch) {
                count -= howMuch;
                wasChanged = true;
                return true;
            }
            return false;
        }
        
        public function get foodId():String {
            return _foodId;
        }
    }
}