package ru.deirel.ferm.components {
    import ru.deirel.ferm.Field;
	/**
     * Information about position of entity on the field
     * @author Deirel
     */
    public class FieldLocationComponent {
        private var _x:int, _y:int;
        private var _field:Field;
        
        public function FieldLocationComponent(x:int, y:int, field:Field) {            
            _x = x;
            _y = y;
            _field = field;
        }
        
        public function get x():int {
            return _x;
        }
        
        public function get y():int {
            return _y;
        }
        
        public function get field():Field {
            return _field;
        }
    }
}