package ru.deirel.ferm.components {
	/**
     * Base class of components which has a progress changing in time once or in cycle
     * @author Deirel
     */
    public class ProgressComponent {
        public var rate:Number;
        
        private var _progress:Number = 0.0;
        private var _isComplete:Boolean = false;
        private var _isCyclic:Boolean = false;
        
        public function ProgressComponent(rate:Number, cyclic:Boolean = false) {
            this.rate = rate;
            _isCyclic = cyclic;
        }
        
        public function update(dt:Number):Boolean {
            if (_isComplete) return false;
            var delta:Number = rate * dt;
            _progress += delta;
            if (_progress < 1.0) return false;
            if (_isCyclic) _progress = _progress % delta;
            else _isComplete = true;
            return true;
        }
        
        public function reset(dt:Number):void {
            _isComplete = false;
            _progress = _isCyclic ? _progress % (rate * dt) : 0.0;
        }
        
        public function get progress():Number {
            return _progress;
        }
        
        public function get isComplete():Boolean {
            return _isComplete;
        }
        
        public function get isCyclic():Boolean {
            return _isCyclic;
        }
    }
}