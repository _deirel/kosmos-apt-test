package ru.deirel.ferm {
    import flash.utils.Dictionary;
	/**
     * Contains the data of entities
     * @author Deirel
     */
    public class EntityRecord {
        static public const TYPE_ANIMAL:String = "animal";
        static public const TYPE_PLANT:String = "plant";
        static public const TYPE_ITEM:String = "item";
        
        static public const WHEAT:String = "wheat";
        static public const CHICKEN:String = "chicken";
        static public const COW:String = "cow";
        static public const EGG:String = "egg";
        static public const MILK:String = "milk";
        
        static private var _records:Dictionary = new Dictionary();
        
        /**
         * Init data about entities by its ids
         * May be loaded externally
         */
        static public function init(completeCallback:Function):void {
            _records[WHEAT] = new EntityRecord(WHEAT, "Пшеница", TYPE_PLANT).setProduceRate(1.0).setSellPrice(1.0);
            _records[CHICKEN] = new EntityRecord(CHICKEN, "Курица", TYPE_ANIMAL).setProduceRate(1.0).setHungerRate(1.0 / 3.0).setProduceItem(EGG);
            _records[COW] = new EntityRecord(COW, "Корова", TYPE_ANIMAL).setProduceRate(0.5).setHungerRate(1.0 / 2.0).setProduceItem(MILK);
            _records[EGG] = new EntityRecord(EGG, "Яйцо", TYPE_ITEM).setSellPrice(2.0);
            _records[MILK] = new EntityRecord(MILK, "Молоко", TYPE_ITEM).setSellPrice(3.0);
            completeCallback();
        }
        
        static public function getById(id:String):EntityRecord {
            return _records[id] as EntityRecord;
        }
        
        /// Unique id of the entity, i.e. "cow", "milk"
        public var id:String;
        
        /// Name of the entity, i.e. "Корова"
        public var name:String;
        
        /// Plant, animal or item
        public var type:String;
        
        /// The rate of production, units / s
        public var produceRate:Number = 0.0;
        
        /// The rate of hunger, entity will become hungry after 'hungerRate' sec
        public var hungerRate:Number = 0.0;
        
        /// Id of product
        public var produceItemId:String;
        
        /// Id of item used in food
        public var foodId:String = WHEAT;
        
        /// Price of the entity
        public var sellPrice:Number = 1.0;
        
        public function EntityRecord(id:String, name:String, type:String) {
            this.id = id;
            this.name = name;
            this.type = type;
            produceItemId = id;
        }
        
        public function setProduceRate(value:Number):EntityRecord {
            this.produceRate = value;
            return this;
        }
        
        public function setHungerRate(value:Number):EntityRecord {
            this.hungerRate = value;
            return this;
        }
        
        public function setProduceItem(id:String):EntityRecord {
            this.produceItemId = id;
            return this;
        }
        
        public function setSellPrice(price:Number):EntityRecord {
            this.sellPrice = price;
            return this;
        }
        
        public function isPlant():Boolean {
            return type == TYPE_PLANT;
        }
    }
}