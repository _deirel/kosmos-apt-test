package ru.deirel.ferm {
    import flash.utils.Dictionary;
    import starling.events.EventDispatcher;
	/**
     * ...
     * @author Deirel
     */
    public class Storage extends EventDispatcher {
        static public const EVENT_CHANGE:String = "change";
        static public const EVENT_CLEAN:String = "clean";
        
        private var _items:Dictionary = new Dictionary();
        private var _isEmpty:Boolean = true;
        
        public function Storage() {
        }
        
        public function add(id:String):void {
            _add(id, 1);
            dispatchEventWith(EVENT_CHANGE);
        }
        
        public function addMany(items:Vector.<StorageRecord>):void {
            for each (var item:StorageRecord in items) _add(item.itemId, item.count);
            dispatchEventWith(EVENT_CHANGE);
        }
        
        private function _add(itemId:String, count:int):void {
            _items[itemId] = int(_items[itemId]) + count;
            _isEmpty = false;
        }
        
        public function take(itemId:String, count:int):Boolean {
            var hasCount:int = int(_items[itemId]);
            if (hasCount >= count) {
                hasCount -= count;
                if (hasCount) _items[itemId] = hasCount; else delete _items[itemId];
                dispatchEventWith(EVENT_CHANGE);
                if (!hasCount) dispatchEventWith(EVENT_CLEAN);
                return true;
            }
            return false;
        }
        
        public function takeAway():Vector.<StorageRecord> {
            var items:Vector.<StorageRecord> = itemsToStorageRecords();
            _items = new Dictionary();
            _isEmpty = true;
            dispatchEventWith(EVENT_CHANGE);
            dispatchEventWith(EVENT_CLEAN);
            return items;
        }
        
        public function get items():Vector.<StorageRecord> {
            return itemsToStorageRecords();
        }
        
        public function get isEmpty():Boolean {
            return _isEmpty;
        }
        
        public function has(itemId:String):Boolean {
            return int(_items[itemId]) > 0;
        }
        
        private function itemsToStorageRecords():Vector.<StorageRecord> {
            var vec:Vector.<StorageRecord> = new Vector.<StorageRecord>();
            for (var itemId:String in _items) vec.push(new StorageRecord(itemId, _items[itemId]));
            return vec;
        }
        
        public function dispose():void {
            removeEventListeners();
        }
    }
}