package ru.deirel.ferm {
    import starling.events.EventDispatcher;
	/**
     * ...
     * @author Deirel
     */
    public class Shop extends EventDispatcher {
        static public const EVENT_CHANGE_MONEY:String = "changeMoney";
        
        private var _money:Number = 0.0;
        
        public function Shop() {
        }
        
        public function sell(items:Vector.<StorageRecord>):void {
            var income:Number = 0.0;
            for each (var record:StorageRecord in items) {
                var item:EntityRecord = EntityRecord.getById(record.itemId);
                income += item.sellPrice * record.count;
            }
            _money += income;
            dispatchEventWith(EVENT_CHANGE_MONEY);
        }
        
        public function get money():Number {
            return _money;
        }
        
        public function dispose():void {
            removeEventListeners();
        }
    }
}