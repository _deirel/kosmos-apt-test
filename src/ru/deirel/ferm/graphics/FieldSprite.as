package ru.deirel.ferm.graphics {
    import ash.core.Entity;
    import feathers.controls.Callout;
    import flash.geom.Rectangle;
    import flash.utils.Dictionary;
    import ru.deirel.ferm.Field;
    import ru.deirel.ferm.components.EntityGraphicsComponent;
    import ru.deirel.ferm.graphics.GUIGraphicsProvider;
    import starling.display.DisplayObject;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.textures.TextureSmoothing;
	/**
     * ...
     * @author Deirel
     */
    public class FieldSprite extends Sprite {
        static private const CELL_SIZE:Number = 16.0;
        static private const CELL_GAP:Number = 1.0;
        
        static public const EVENT_ENTITY_TOUCH:String = "entityTouch";
        static public const EVENT_CELL_TOUCH:String = "cellTouch";
        
        private var _field:Field;
        private var _cells:Dictionary = new Dictionary(true);
        
        public function FieldSprite(field:Field, graphicsProvider:GUIGraphicsProvider) {
            _field = field;
            initGraphics(graphicsProvider);
            initEvents();
        }
        
        private function initGraphics(graphicsProvider:GUIGraphicsProvider):void {
            var bg:Image = new Image(graphicsProvider.getTexture("field_bg_slice9"));
            bg.scale9Grid = new Rectangle(3, 3, 1, 1);
            bg.textureSmoothing = TextureSmoothing.NONE;
            bg.touchable = false;
            addChild(bg);
            
            var bgFill:Image = new Image(graphicsProvider.getTexture("field_bg_fill"));
            bgFill.tileGrid = new Rectangle(0, 0, 6, 6);
            bgFill.textureSmoothing = TextureSmoothing.NONE;
            bgFill.touchable = false;
            bgFill.x = bgFill.y = 3;
            addChild(bgFill);
            
            var fillWidth:Number = _field.width * CELL_SIZE + (_field.width - 1) * CELL_GAP;
            var fillHeight:Number = _field.height * CELL_SIZE + (_field.height - 1) * CELL_GAP;
            
            bg.width = 6 + fillWidth;
            bg.height = 6 + fillHeight;
            bgFill.width = fillWidth;
            bgFill.height = fillHeight;
            
            for (var j:int = 0; j < _field.height; j++)
            for (var i:int = 0; i < _field.width; i++) {
                var cellImage:FieldCellSprite = new FieldCellSprite(i, j, graphicsProvider);
                cellImage.x = _localX(i);
                cellImage.y = _localY(j);
                addChild(cellImage);
                _cells[_id(i, j)] = cellImage;
            }
        }
        
        private function initEvents():void {
            addEventListener(TouchEvent.TOUCH, onTouch);
            _field.addEventListener(Field.EVENT_ADD_ENTITY, onFieldAddEntity);
            _field.addEventListener(Field.EVENT_REMOVE_ENTITY, onFieldRemoveEntity);
        }
        
        private function onFieldAddEntity(e:Event):void {
            var entity:Entity = e.data.entity;
            var x:int = e.data.x;
            var y:int = e.data.y;
            var graphics:EntityGraphicsComponent = entity.get(EntityGraphicsComponent);
            if (graphics) {
                graphics.fieldX = x;
                graphics.fieldY = y;
                graphics.x = _localX(x);
                graphics.y = _localY(y);
                addChild(graphics);
            }
        }
        
        private function onFieldRemoveEntity(e:Event):void {
            var entity:Entity = e.data.entity;
            var graphics:EntityGraphicsComponent = entity.get(EntityGraphicsComponent);
            if (graphics) graphics.removeFromParent(true);
        }
        
        private function onTouch(e:TouchEvent):void {
            var touch:Touch = e.getTouch(this, TouchPhase.ENDED);
            if (touch) {
                var entity:Entity;
                if (e.target is EntityGraphicsComponent) {
                    var graphicsComponent:EntityGraphicsComponent = EntityGraphicsComponent(e.target);
                    entity = _field.getEntity(graphicsComponent.fieldX, graphicsComponent.fieldY);
                    dispatchEventWith(EVENT_ENTITY_TOUCH, false, { entity:entity, x:graphicsComponent.fieldX, y:graphicsComponent.fieldY });
                } else if (e.target is FieldCellSprite) {
                    var cellImage:FieldCellSprite = FieldCellSprite(e.target);
                    entity = _field.getEntity(cellImage.fieldX, cellImage.fieldY);
                    if (entity) {
                        dispatchEventWith(EVENT_ENTITY_TOUCH, false, { entity:entity, x:cellImage.fieldX, y:cellImage.fieldY });
                    } else {
                        dispatchEventWith(EVENT_CELL_TOUCH, false, { x:cellImage.fieldX, y:cellImage.fieldY });
                    }
                }
            }
        }
        
        private function _localX(x:int):Number {
            return (x + 0.5) * CELL_SIZE + (x - 1) * CELL_GAP + 4.0;;
        }
        
        private function _localY(y:int):Number {
            return (y + 0.5) * CELL_SIZE + (y - 1) * CELL_GAP + 4.0;;
        }
        
        private function _id(x:int, y:int):String {
            return x + ":" + y;
        }
        
        public function createCalloutOnCell(x:int, y:int, content:DisplayObject):Callout {
            return Callout.show(content, _cells[_id(x, y)], null, false);
        }
        
        override public function dispose():void {
            _field = null;
            super.dispose();
        }
    }
}