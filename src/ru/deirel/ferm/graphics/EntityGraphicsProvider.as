package ru.deirel.ferm.graphics {
    import ru.deirel.ferm.EntityRecord;
    import starling.textures.TextureAtlas;
	/**
     * ...
     * @author Deirel
     */
    public class EntityGraphicsProvider {
        private var _atlas:TextureAtlas;
        
        public function EntityGraphicsProvider(atlas:TextureAtlas) {
            _atlas = atlas;
        }
        
        public function getGraphicsByRecord(rec:EntityRecord):EntityGraphicsSprite {
            return new EntityGraphicsSprite(rec.id + "_", _atlas);
        }
    }
}