package ru.deirel.ferm.graphics {
    import ash.core.Entity;
    import ru.deirel.ferm.EntityRecord;
	/**
     * ...
     * @author Deirel
     */
    public class EntityAction {
        static public const FEED:String = "feed";
        static public const HARVEST:String = "harvest";
        static public const REMOVE_ENTITY:String = "removeEntity";
        
        public var type:String;
        public var entity:Entity;
        public var record:EntityRecord;
    }
}