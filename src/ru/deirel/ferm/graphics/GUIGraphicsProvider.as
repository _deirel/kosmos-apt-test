package ru.deirel.ferm.graphics {
    import starling.display.Image;
    import starling.textures.Texture;
    import starling.textures.TextureAtlas;
    import starling.textures.TextureSmoothing;
	/**
     * ...
     * @author Deirel
     */
    public class GUIGraphicsProvider {
        private var _atlas:TextureAtlas;
        
        public function GUIGraphicsProvider(atlas:TextureAtlas) {
            _atlas = atlas;
        }
        
        public function getTexture(id:String):Texture {
            return _atlas.getTexture(id);
        }
        
        public function getIcon(id:String, scale:Number = 1.0):Image {
            var tex:Texture = _atlas.getTexture("ico_" + id);
            if (!tex) return null;
            var img:Image = new Image(tex);
            img.textureSmoothing = TextureSmoothing.NONE;
            img.scale = scale;
            return img;
        }
    }
}