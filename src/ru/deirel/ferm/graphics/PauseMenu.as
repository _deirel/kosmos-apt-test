package ru.deirel.ferm.graphics {
    import feathers.controls.List;
    import feathers.controls.Panel;
    import feathers.core.PopUpManager;
    import feathers.data.ListCollection;
    import feathers.motion.Fade;
    import ru.deirel.ferm.engine.Director;
    import ru.deirel.ferm.engine.IBackButtonHandler;
    import ru.deirel.ferm.screens.ScreenName;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class PauseMenu extends Panel implements IBackButtonHandler {
        public function PauseMenu() {
        }
        
        override protected function initialize():void {
            super.initialize();
            
            title = "Меню";
            
            var menu:List = new List();
            var items:Array = [
                { text:"Возобновить игру" },
                { text:"Выход в главное меню", callback:mainMenuCb }];
            if (Director.instance.isMobile) items.push({ text:"Выход", callback:exitCb });
            menu.dataProvider = new ListCollection(items);
            menu.itemRendererProperties.labelField = "text";
            menu.name = "menu";
            addChild(menu); 
            
            menu.addEventListener(Event.CHANGE, onMenuListChange);
            
            Director.instance.backButtonProcessor.addHandler(this);
        }
        
        private function onMenuListChange(e:Event):void {
            var menu:List = List(e.currentTarget);
            if (menu.selectedItem.callback) menu.selectedItem.callback();
            PopUpManager.removePopUp(this, true);
        }
        
        private function mainMenuCb():void {
            Director.instance.screenNavigator.showScreen(ScreenName.MENU, Fade.createCrossfadeTransition());
        }
        
        private function exitCb():void {
            Director.instance.end();
        }
        
        public function onBackButton():void {
            PopUpManager.removePopUp(this, true);
        }
        
        override public function dispose():void {
            Director.instance.backButtonProcessor.removeHandler(this);
            super.dispose();
        }
    }
}