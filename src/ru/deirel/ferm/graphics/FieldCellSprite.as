package ru.deirel.ferm.graphics {
    import starling.display.Image;
    import starling.textures.TextureSmoothing;
	/**
     * ...
     * @author Deirel
     */
    public class FieldCellSprite extends Image {
        private var _fieldX:int, _fieldY:int;
        
        public function FieldCellSprite(x:int, y:int, provider:GUIGraphicsProvider) {
            super(provider.getTexture("field_cell"));
            _fieldX = x;
            _fieldY = y;
            
            textureSmoothing = TextureSmoothing.NONE;
            alignPivot();
        }
        
        public function get fieldX():int {
            return _fieldX;
        }
        
        public function get fieldY():int {
            return _fieldY;
        }
    }
}