package ru.deirel.ferm.graphics {
    import feathers.controls.List;
    import feathers.data.ListCollection;
    import ru.deirel.ferm.EntityRecord;
    import ru.deirel.ferm.engine.Director;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class CellCallout extends List {
        private var _cb:Function;
        private var _cellX:int, _cellY:int;
        
        /**
         * Callback's argument is CellAction instance
         */
        public function CellCallout(provider:GUIGraphicsProvider, cb:Function, cellX:int, cellY:int) {
            _cb = cb;
            _cellX = cellX;
            _cellY = cellY;
            dataProvider = new ListCollection([
                getListItem(provider, EntityRecord.WHEAT),
                getListItem(provider, EntityRecord.COW),
                getListItem(provider, EntityRecord.CHICKEN)
            ]);
            itemRendererProperties.labelField = "text";
            itemRendererProperties.iconField = "icon";
            addEventListener(Event.CHANGE, onChange);
        }
        
        private function getListItem(provider:GUIGraphicsProvider, entityId:String):Object {
            var record:EntityRecord = EntityRecord.getById(entityId);
            return { text:record.name, icon:provider.getIcon(record.id, Director.SCALE / 2.0), id:record.id };
        }
        
        private function onChange(e:Event):void {
            if (!selectedItem) return;
            var action:CellAction = new CellAction();
            action.cellX = _cellX;
            action.cellY = _cellY;
            action.entityId = selectedItem.id;
            _cb(action);
        }
        
        override public function dispose():void {
            _cb = null;
            super.dispose();
        }
    }
}