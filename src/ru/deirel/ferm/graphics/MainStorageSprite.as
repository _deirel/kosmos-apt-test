package ru.deirel.ferm.graphics {
    import feathers.controls.Button;
    import feathers.controls.List;
    import feathers.data.ListCollection;
    import feathers.layout.AnchorLayoutData;
    import ru.deirel.ferm.EntityRecord;
    import ru.deirel.ferm.Storage;
    import ru.deirel.ferm.StorageRecord;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class MainStorageSprite extends List {
        static public const WIDTH:Number = 200.0;
        
        static public const EVENT_SELL:String = "sell";
        
        private var _itemGraphicsProvider:ItemGraphicsProvider;
        private var _guiGraphicsProvider:GUIGraphicsProvider;
        private var _storage:Storage;
        
        public function MainStorageSprite(storage:Storage, itemGraphicsProvider:ItemGraphicsProvider, guiGraphicsProvider:GUIGraphicsProvider) {
            _storage = storage;
            _itemGraphicsProvider = itemGraphicsProvider;
            _guiGraphicsProvider = guiGraphicsProvider;
            
            width = WIDTH;
            
            isSelectable = false;
            
            itemRendererProperties.labelField = "text";
            itemRendererProperties.iconField = "icon";
            itemRendererProperties.accessoryField = "sellButton";
        }
        
        override protected function initialize():void {
            super.initialize();
            
            _storage.addEventListener(Storage.EVENT_CHANGE, update);
            update();
        }
        
        private function update(e:Event = null):void {
            var items:Array = [];
            var storageItems:Vector.<StorageRecord> = _storage.items;
            for each (var item:StorageRecord in storageItems) {
                var record:EntityRecord = EntityRecord.getById(item.itemId);
                var listItem:Object = {};
                listItem.icon = _itemGraphicsProvider.getGraphicsById(item.itemId, false, true);
                listItem.text = record.name + " (" + item.count + ")";
                listItem.sellButton = createSellButton(item.itemId);
                items.push(listItem);
            }
            dataProvider = new ListCollection(items);
        }
        
        private function createSellButton(itemId:String):Button {
            var button:_SellButton = new _SellButton();
            button.defaultIcon = _guiGraphicsProvider.getIcon("sell", 3.0);
            button.itemId = itemId;
            button.addEventListener(Event.TRIGGERED, onSellButtonTriggered);
            return button;
        }
        
        private function onSellButtonTriggered(e:Event):void {
            var button:_SellButton = e.target as _SellButton;
            if (button) dispatchEventWith(EVENT_SELL, false, { id:button.itemId });
        }
        
        override public function dispose():void {
            if (_storage) {
                _storage.removeEventListener(Storage.EVENT_CHANGE, update);
                _storage = null;
            }
            super.dispose();
        }
    }
}

import feathers.controls.Button;

class _SellButton extends Button {
    public var itemId:String;
}