package ru.deirel.ferm.graphics {
    import starling.core.Starling;
    import starling.display.MovieClip;
    import starling.display.Sprite;
    import starling.textures.Texture;
    import starling.textures.TextureAtlas;
    import starling.textures.TextureSmoothing;
	/**
     * It's like an movie clip when every frame is movie clip itself.
     * Can be in many states.
     * Textures are from atlas with names: <id>_<stateNum>_<frame>, i.e.:
     *      wheat_0_00
     *      wheat_0_01  --  animation for wheat in state 0
     *      wheat_1_00
     *      wheat_1_01  --  animation for wheat in state 1 etc
     * @author Deirel
     */
    public class EntityGraphicsSprite extends Sprite {
        private var _movies:Vector.<MovieClip> = new Vector.<MovieClip>();
        private var _current:MovieClip;
        private var _currentId:int = -1;
        
        public function EntityGraphicsSprite(prefix:String, atlas:TextureAtlas) {
            var i:int = 0;
            var flag:Boolean = true;
            while (flag) {
                var textures:Vector.<Texture> = atlas.getTextures(prefix + i + "_");
                if (textures && textures.length) _movies[i++] = createMovieClip(textures); else flag = false;
            }
            if (_movies.length) setMovieClip(0);
        }
        
        private function createMovieClip(textures:Vector.<Texture>):MovieClip {
            // MovieClip fps that the animation time was 1 sec.
            var mov:MovieClip = new MovieClip(textures, textures.length);
            mov.textureSmoothing = TextureSmoothing.NONE;
            mov.alignPivot();
            return mov;
        }
        
        private function setMovieClip(id:int):void {
            if (id == _currentId) return;
            if (_current) {
                Starling.juggler.remove(_current);
                removeChild(_current);
                _current = null;
                _currentId = -1;
            }
            if (id < 0 || id > _movies.length - 1) return;
            _current = _movies[id];
            _currentId = id;
            addChild(_current);
            Starling.juggler.add(_current);
        }
        
        public function set progress(value:Number):void {
            setMovieClip(Math.round(value * (_movies.length - 1)));
        }
        
        override public function dispose():void {
            if (_current) Starling.juggler.remove(_current);
            super.dispose();
        }
    }
}