package ru.deirel.ferm.graphics {
    import feathers.controls.Label;
    import feathers.controls.LayoutGroup;
    import feathers.controls.Panel;
    import feathers.layout.AnchorLayout;
    import feathers.layout.AnchorLayoutData;
    import ru.deirel.ferm.Shop;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class MoneyPanel extends Panel {
        private var _shop:Shop;
        private var _label:Label;
        
        public function MoneyPanel(shop:Shop) {
            _shop = shop;
            
            width = 150.0;
            height = 40.0;
            layout = new AnchorLayout();
            
            headerFactory = function():LayoutGroup { return new LayoutGroup(); };
        }
        
        override protected function initialize():void {
            super.initialize();
            
            _label = new Label();
            _label.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0.0, 0.0);
            addChild(_label);
            
            _shop.addEventListener(Shop.EVENT_CHANGE_MONEY, onChangeMoney);
            onChangeMoney();
        }
        
        private function onChangeMoney(e:Event = null):void {
            _label.text = "Деньги: " + _shop.money;
        }
        
        override public function dispose():void {
            if (_shop) {
                _shop.removeEventListener(Shop.EVENT_CHANGE_MONEY, onChangeMoney);
                _shop = null;
            }
            super.dispose();
        }
    }
}