package ru.deirel.ferm.graphics {
    import starling.display.DisplayObject;
    import starling.display.Image;
    import starling.textures.TextureAtlas;
    import starling.textures.TextureSmoothing;
	/**
     * ...
     * @author Deirel
     */
    public class ItemGraphicsProvider {
        private var _atlas:TextureAtlas;
        
        public function ItemGraphicsProvider(atlas:TextureAtlas) {
            _atlas = atlas;
        }
        
        public function getGraphicsById(id:String, align:Boolean = true, big:Boolean = false):DisplayObject {
            var img:Image = new Image(_atlas.getTexture((big ? "i_b_" : "i_") + id));
            if (align) img.alignPivot();
            img.textureSmoothing = TextureSmoothing.NONE;
            return img;
        }
    }
}