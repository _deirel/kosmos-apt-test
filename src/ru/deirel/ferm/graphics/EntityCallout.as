package ru.deirel.ferm.graphics {
    import ash.core.Entity;
    import feathers.controls.List;
    import feathers.data.ListCollection;
    import feathers.events.FeathersEventType;
    import ru.deirel.ferm.EntityRecord;
    import ru.deirel.ferm.Game;
    import ru.deirel.ferm.Storage;
    import ru.deirel.ferm.components.EntityRecordComponent;
    import ru.deirel.ferm.components.FeederComponent;
    import ru.deirel.ferm.components.StorageComponent;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class EntityCallout extends List {
        private var _cb:Function;
        
        private var _entity:Entity;
        private var _record:EntityRecord;
        private var _entityStorage:Storage;
        private var _hasFeeder:Boolean;
        private var _gameStorage:Storage;
        
        public function EntityCallout(provider:GUIGraphicsProvider, cb:Function, entity:Entity, game:Game) {
            _cb = cb;
            _entity = entity;
            _entityStorage = tryGetStorage(entity);
            _gameStorage = game.storage;
            _record = EntityRecordComponent(entity.get(EntityRecordComponent)).record;
            _hasFeeder = entity.has(FeederComponent);
            initEvents();
            updateItems();
            itemRendererProperties.labelField = "text";
            itemRendererProperties.iconField = "icon";
            addEventListener(Event.CHANGE, onChange);
        }
        
        private function tryGetStorage(e:Entity):Storage {
            var comp:StorageComponent = e.get(StorageComponent);
            return comp ? comp.storage : null;
        }
        
        private function initEvents():void {
            _gameStorage.addEventListener(Storage.EVENT_CHANGE, updateItems);
            if (_entityStorage) _entityStorage.addEventListener(Storage.EVENT_CHANGE, updateItems);
        }
        
        private function updateItems(e:Event = null):void {
            var items:Array = [{ text: "Убрать", type:EntityAction.REMOVE_ENTITY }];
            if (_entityStorage && !_entityStorage.isEmpty) items.push({ text:"Собрать " + (_record.isPlant() ? "урожай" : "продукцию"), type:EntityAction.HARVEST });
            if (_hasFeeder && _gameStorage.has(_record.foodId)) items.push({ text:"Накормить", type:EntityAction.FEED });
            if (!dataProvider) dataProvider = new ListCollection(items);
            else dataProvider.data = items;
            // Here is a workaround for the callout automatic resize
            dispatchEventWith(FeathersEventType.RESIZE);
        }
        
        private function onChange(e:Event):void {
            if (!selectedItem) return;
            var action:EntityAction = new EntityAction();
            action.type = selectedItem.type;
            action.entity = _entity;
            action.record = _record;
            _cb(action);
        }
        
        override public function dispose():void {
            _gameStorage.removeEventListener(Storage.EVENT_CHANGE, updateItems);
            if (_entityStorage) _entityStorage.removeEventListener(Storage.EVENT_CHANGE, updateItems);
            
            _gameStorage = null;
            _entityStorage = null;
            _cb = null;
            _entity = null;
            
            super.dispose();
        }
    }
}