package ru.deirel.ferm {
    import ash.core.Node;
    import starling.display.DisplayObject;
    import starling.display.DisplayObjectContainer;
	/**
     * ...
     * @author Deirel
     */
    public class Utils {
        /**
         * Search the child by name recursively
         */
        static public function getChildByName(container:DisplayObjectContainer, name:String):DisplayObject {
            var child:DisplayObject = container.getChildByName(name);
            if (!child) for (var i:int = 0; i < container.numChildren; i++) {
                var childContainer:DisplayObjectContainer = container.getChildAt(i) as DisplayObjectContainer;
                if (childContainer) child = getChildByName(childContainer, name);
                if (child) return child;
            }
            return child;
        }
        
        static public function emptyNodeFunction(node:Node, dt:Number):void {
        }
        
        static public function dispose(d:Object):* {
            if (d && d.hasOwnProperty("dispose") && d.dispose is Function) d.dispose();
            return null;
        }
    }
}