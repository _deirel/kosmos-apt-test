package {
    import feathers.controls.ScreenNavigatorItem;
    import feathers.motion.Fade;
    import ru.deirel.ferm.engine.Director;
    import ru.deirel.ferm.screens.GameScreen;
    import ru.deirel.ferm.screens.InitScreen;
    import ru.deirel.ferm.screens.MenuScreen;
    import ru.deirel.ferm.screens.ScreenName;
    import starling.core.Starling;
    import starling.display.Sprite;
    import starling.events.Event;
	/**
     * ...
     * @author Deirel
     */
    public class Initialize extends Sprite {
        private var _starling:Starling;
        
        public function Initialize() {
            _starling = Starling.current;
            _starling.showStats = true;
            _starling.addEventListener(Event.ROOT_CREATED, function onRootCreated(e:Event):void {
                _starling.removeEventListener(Event.ROOT_CREATED, onRootCreated);
                init();
            });
        }
        
        private function init():void {
            Director.instance.init(new NativeCapabilities());
            
            // Setup scale for different resolutions
            var fixedHeight:int = Director.instance.isMobile ? 400 : 600;
            _starling.stage.stageWidth = Math.ceil(_starling.viewPort.width / _starling.viewPort.height * fixedHeight);
            _starling.stage.stageHeight = fixedHeight;
            
            addChild(Director.instance.screenNavigator);
            Director.instance.screenNavigator.addScreen(ScreenName.INIT, new ScreenNavigatorItem(InitScreen));
            Director.instance.screenNavigator.addScreen(ScreenName.MENU, new ScreenNavigatorItem(MenuScreen));
            Director.instance.screenNavigator.addScreen(ScreenName.GAME, new ScreenNavigatorItem(GameScreen));
            
            Director.instance.screenNavigator.showScreen(ScreenName.INIT, Fade.createFadeInTransition(0.4));
        }
    }
}