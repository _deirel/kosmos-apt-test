package {
    import feathers.themes.MetalWorksMobileTheme;
    import ru.deirel.ferm.engine.INativeCapabilities;
	/**
     * ...
     * @author Deirel
     */
    public class NativeCapabilities implements INativeCapabilities {
        public function init():void {
            // Here we can instantiate any theme
            new MetalWorksMobileTheme();
        }
        
        public function isMobile():Boolean {
            return false;
        }
        
        public function closeApp():void {
        }
    }
}