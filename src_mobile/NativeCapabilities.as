package {
    import feathers.themes.MetalWorksMobileTheme;
    import flash.desktop.NativeApplication;
    import ru.deirel.ferm.engine.INativeCapabilities;
	/**
     * ...
     * @author Deirel
     */
    public class NativeCapabilities implements INativeCapabilities {
        public function init():void {
            new MetalWorksMobileTheme();
        }
        
        public function isMobile():Boolean {
            return true;
        }
        
        public function closeApp():void {
            NativeApplication.nativeApplication.exit();
        }
    }
}