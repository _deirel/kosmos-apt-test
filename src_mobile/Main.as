package{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.ui.Multitouch;
    import flash.ui.MultitouchInputMode;
    import starling.core.Starling;
	
	/**
	 * ...
	 * @author Deirel
	 */
	public class Main extends Sprite {
		public function Main() {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			new Starling(Initialize, stage).start();
		}
        
        private function activate(e:Event):void {
            stage.removeEventListener(Event.ACTIVATE, activate);
            Starling.current.start();
        }
		
		private function deactivate(e:Event):void {
			Starling.current.stop();
            stage.addEventListener(Event.ACTIVATE, activate);
		}
	}
}